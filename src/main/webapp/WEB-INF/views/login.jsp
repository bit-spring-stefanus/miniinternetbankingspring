<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://www.springframework.org/tags"%>
<html>
<head>
    <title><t:message code="app.title"/></title>
<%@ page isELIgnored="false" %>
<link href="<c:url value="/assets/vendors/mdi/css/materialdesignicons.min.css" />" rel="stylesheet">
<link href="<c:url value="/assets/vendors/css/vendor.bundle.base.css" />" rel="stylesheet">
<link href="<c:url value="/assets/css/style.css" />" rel="stylesheet">
<link href="<c:url value="/assets/images/favicon.png" />" rel="shortcut icon">
</head>
<body onload='document.loginForm.username.focus();'>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
              	<!-- Dropdown for selecting language -->
		         <div class="dropdown float-right">
		            <button class="btn-sm btn-outline-info dropdown-toggle" type="button" id="dropdownMenuButton"
		               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><t:message code="app.lang.title"/></button>
	              	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	               		<a class="dropdown-item" href="?lang=en"><t:message code="app.lang.english"/></a> 
	               		<a class="dropdown-item" href="?lang=id"><t:message code="app.lang.indonesia"/></a>
	            	</div>
		         </div>
                <div class="brand-logo">
                  <img src="<c:url value="/assets/images/bca-logo.svg" />">
                </div>
                <h4><t:message code="app.page.login.judul"/></h4>
                <h6 class="font-weight-light"><t:message code="app.page.login.sub"/></h6>
                <c:if test="${not empty pesanError}">
                	<h6 class="text-danger" style="font-weight: bold;">${pesanError}</h6>
                </c:if>
                <form name='login' action="login" method='POST' class="pt-3">
                  <div class="form-group">
                    <input type="text" name="username" value="" class="form-control form-control-lg" id="exampleInputEmail1"  placeholder="Username" autocomplete="off" required="true">
                  </div>
                  <div class="form-group">
                    <input type="password" name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" autocomplete="off" required="true">
                  </div>
                  <div class="mt-3">
                  	<input type="submit" name="submit" value="<t:message code="app.page.login.button"/>" class="btn btn-block btn-gradient-info btn-lg font-weight-medium auth-form-btn"/>
                  </div>
                  <div class="my-2 d-flex justify-content-between align-items-center">
                    <a href="#" class="auth-link text-black"><t:message code="app.page.login.lupa"/></a>
                  </div>
                  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<c:url value="/assets/vendors/js/vendor.bundle.base.js"/>"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="<c:url value="/assets/js/off-canvas.js"/>"></script>
    <script src="<c:url value="/assets/js/hoverable-collapse.js"/>"></script>
    <script src="<c:url value="/assets/js/misc.js"/>"></script>
    <!-- endinject -->
</body>
</html>