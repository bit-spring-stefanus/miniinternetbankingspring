<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://www.springframework.org/tags"%>
<%@ page isELIgnored="false"%>
<!-- partial:../../partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
	    <c:if test="${pageContext.request.servletPath == '/WEB-INF/views/welcome.jsp'}">
	        <li class="nav-item nav-profile">
	            <a href="#" class="nav-link">
	                <div class="nav-profile-text d-flex flex-column">
	                    <span class="font-weight-bold mb-2">${nasabah.nama}</span>
	                    <span class="text-secondary text-small">Nasabah Solitare</span>
	                </div>
	                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
	            </a>
	        </li>
	    </c:if>
        <li class="nav-item ${pageContext.request.servletPath=='/WEB-INF/views/welcome.jsp' ? 'active' : ''}" >
            <a class="nav-link" href="${pageContext.request.contextPath}/welcome">
                <span class="menu-title"><t:message code="app.part.sidebar.dashboard"/></span>
                <i class="mdi mdi-home text-info menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/pembelian/voucher">
                <span class="menu-title"><t:message code="app.part.sidebar.pembelian"/></span>
                <i class="mdi mdi-shopping text-info menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/pembayaran/telepon">
                <span class="menu-title"><t:message code="app.part.sidebar.pembayaran"/></span>
                <i class="mdi mdi-cellphone text-info menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title"><t:message code="app.part.sidebar.kartukredit"/></span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-credit-card text-info menu-icon"></i>
            </a>
            <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="${pageContext.request.contextPath}/kartu-kredit/tagihan"><t:message code="app.part.sidebar.tagihan"/></a></li>
                    <li class="nav-item"> <a class="nav-link" href="${pageContext.request.contextPath}/kartu-kredit/transaksi"><t:message code="app.part.sidebar.riwayat"/></a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">
                <span class="menu-title"><t:message code="app.part.sidebar.administrasi"/></span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-settings text-info menu-icon"></i>
            </a>
            <div class="collapse" id="general-pages">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="${pageContext.request.contextPath}/pengaturan/ganti-password"><t:message code="app.part.sidebar.katasandi"/></a></li>
                    <li class="nav-item"> <a class="nav-link" href="${pageContext.request.contextPath}/pengaturan/ganti-bahasa"><t:message code="app.part.sidebar.bahasa"/></a></li>
                </ul>
            </div>
        </li>
    </ul>
</nav>
<!-- partial -->