<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
  <head>
<%@ page isELIgnored="false"%>
    <!-- Required meta tags -->
    <title>Mini Internet Banking</title>
    <!-- plugins:css -->
	<link href="<c:url value="/assets/vendors/mdi/css/materialdesignicons.min.css" />" rel="stylesheet">
	<link href="<c:url value="/assets/vendors/css/vendor.bundle.base.css" />" rel="stylesheet">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
	<link href="<c:url value="/assets/css/style.css" />" rel="stylesheet">
    <!-- End layout styles -->
	<link href="<c:url value="/assets/images/favicon.png" />" rel="shortcut icon">
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:../../views/partials/_navbar.html -->
      <jsp:include page="../views/partials/_navbar.jsp">
      	<jsp:param name="nasabah" value="${nasabah}" />
      </jsp:include>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../views/partials/_sidebar.html -->
	    <jsp:include page="../views/partials/_sidebar.jsp">
	    	<jsp:param name="nasabah" value="${nasabah}" />
	    </jsp:include>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
          
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-info text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span> Dashboard </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-info align-middle"></i>
                  </li>
                </ul>
              </nav>
            </div>
            
            
            <div class="row">
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-info card-img-holder text-white">
                  <div class="card-body">
                    <img src="assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                    <h4 class="font-weight-normal mb-3"><t:message code="app.lang.title"/><i class="mdi mdi-chart-line mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">RP 75.000</h2>
                    <h6 class="card-text">Increased by 12%</h6>
                  </div>
                </div>
              </div>
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-success card-img-holder text-white">
                  <div class="card-body">
                    <img src="assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                    <h4 class="font-weight-normal mb-3"> Transaksi Tahunan <i class="mdi mdi-bookmark-outline mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">RP 95.320.000</h2>
                    <h6 class="card-text">Decreased by 46%</h6>
                  </div>
                </div>
              </div>
              <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-danger card-img-holder text-white">
                  <div class="card-body">
                    <img src="assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                    <h4 class="font-weight-normal mb-3">Tagihan Belum Dibayar <i class="mdi mdi-diamond mdi-24px float-right"></i>
                    </h4>
                    <h2 class="mb-5">6</h2>
                    <h6 class="card-text">Increased by 100%</h6>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<c:url value="/assets/vendors/js/vendor.bundle.base.js"/>"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="<c:url value="/assets/js/off-canvas.js"/>"></script>
    <script src="<c:url value="/assets/js/hoverable-collapse.js"/>"></script>
    <script src="<c:url value="/assets/js/misc.js"/>"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
	    $(document).ready(function() {
	        var interval = setInterval(function() {
	            var momentNow = moment();
	            $('#date-part').html(momentNow.format('YYYY MMMM DD') + ' '
	                                + momentNow.format('dddd')
	                                 .substring(0,3).toUpperCase());
	            $('#time-part').html(momentNow.format('A hh:mm:ss'));
	        }, 100);
	    });
	</script>
    <!-- End custom js for this page -->
  </body>
</html>