<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
  <head>
<%@ page isELIgnored="false"%>
    <!-- Required meta tags -->
    <title>Mini Internet Banking</title>
    <!-- plugins:css -->
	<link href="<c:url value="/assets/vendors/mdi/css/materialdesignicons.min.css" />" rel="stylesheet">
	<link href="<c:url value="/assets/vendors/css/vendor.bundle.base.css" />" rel="stylesheet">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
	<link href="<c:url value="/assets/css/style.css" />" rel="stylesheet">
    <!-- End layout styles -->
	<link href="<c:url value="/assets/images/favicon.png" />" rel="shortcut icon">
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <jsp:include page="../views/partials/_navbar.jsp">
      	<jsp:param name="nasabah" value="${nasabah}" />
      </jsp:include>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
	    <jsp:include page="../views/partials/_sidebar.jsp">
	    	<jsp:param name="nasabah" value="${nasabah}" />
	    </jsp:include>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">KARTU KREDIT - TAGIHAN</h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Kartu Kredit</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Tagihan</li>
                </ol>
              </nav>
            </div>
	        <div class="row" id="proBanner">
	           <div class="col-12">
	           <c:if test="${not empty pesanError[1] || not empty pesanError[0]}">
	             <span class="d-flex align-items-center purchase-popup">
	               <p>${pesanError[0]} ${pesanError[1]}</p>
	             </span>
	           </c:if>
	         </div>
              <div class="col-sm-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">KARTU KREDIT</h4>
                    <p class="card-description"> Pembayaran Tagihan Kartu Kredit </p>
                    <form name='tagihanCC' action="tagihan-submit" method='POST' class="forms-sample">
                      <div class="form-group row">
                        <label for="exampleInputMobile" class="col-sm-3 col-form-label">PERIODE</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="periode" name="periode" value="${periode}" disabled>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputMobile" class="col-sm-3 col-form-label">TOTAL TAGIHAN</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="toTagihan" name="toTagihan" value="<fmt:formatNumber pattern="#,###,###,###" value="${toTagihanAll}" />" disabled>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputMobile" class="col-sm-3 col-form-label">TOTAL TAGIHAN BELUM DIBAYAR</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="toTagihan" name="toTagihan" value="<fmt:formatNumber pattern="#,###,###,###" value="${toTagihan}" />" disabled>
                        </div>
                      </div>
		                <c:if test="${tagihanSdhDibayar > 0}">
	                      <div class="form-group row">
	                        <label for="exampleInputMobile" class="col-sm-3 col-form-label">TAGIHAN SUDAH DIBAYAR</label>
	                        <div class="col-sm-9">
	                          <input type="text" class="form-control" id="miBayar" name="miBayarText" value="<fmt:formatNumber pattern="#,###,###,###" value="${tagihanSdhDibayar}" />" disabled>
	                        </div>
	                      </div>
		                </c:if>
                      <div class="form-group row">
                        <label for="exampleInputMobile" class="col-sm-3 col-form-label">MINIMAL BAYAR</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="miBayar" name="miBayarText" value="<fmt:formatNumber pattern="#,###,###,###" value="${miBayar}" />" disabled>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputMobile" class="col-sm-3 col-form-label">TANGGAL JATUH TEMPO</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="tgJthTempo" name="tgJthTempo" value="<fmt:formatDate value="${tgJthTempo}" pattern="dd MMMM YYYY" />" disabled>
                        </div>
                      </div>
                      
                      <table class="table table-bordered">
	                      <thead>
	                        <tr>
	                          <th> # </th>
	                          <th> Nama Merchant </th>
	                          <th> Tanggal Transaksi </th>
	                          <th> Nominal Transaksi </th>
	                        </tr>
	                      </thead>
	                      <tbody>
	                      
	                      	<c:forEach items="${listCCTransaksi}" var="transaksi" varStatus="loop">
	                        <tr>
	                          <td> ${loop.count} </td>
	                          <td> ${transaksi.merchant} </td>
	                          <td> ${transaksi.tanggal} </td>
	                          <td><fmt:formatNumber pattern="#,###,###,###" value="${transaksi.nominal}" /></td>
	                        </tr>
	                        </c:forEach>
	                        
	                      </tbody>
                      </table>
                      
                      <div class="form-group row mt-4">
                        <label for="exampleInputMobile" class="col-sm-3 col-form-label">NILAI BAYAR</label>
                        <div class="col-sm-9">
                          <input type="number" class="form-control" id="nominalBayar" name="nominalBayar" placeholder="NOMINAL BAYAR" autocomplete="off" required="true">
                        </div>
                      </div>
                      <input type="hidden" id="periode" name="periode" value="${periode}">
                      <input type="hidden" id="intBulan" name="intBulan" value="${intBulan}">
                      <input type="hidden" id="miBayar" name="miBayar" value="${miBayar}">
                      <input type="hidden" id="id_tagihan" name="id_tagihan" value="${id_tagihan}">
                      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                      <input type="submit" name="submit" value="Submit" class="btn btn-gradient-info float-right"/>
                    </form>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div></div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<c:url value="/assets/vendors/js/vendor.bundle.base.js"/>"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="<c:url value="/assets/js/off-canvas.js"/>"></script>
    <script src="<c:url value="/assets/js/hoverable-collapse.js"/>"></script>
    <script src="<c:url value="/assets/js/misc.js"/>"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- End custom js for this page -->
  </body>
</html>