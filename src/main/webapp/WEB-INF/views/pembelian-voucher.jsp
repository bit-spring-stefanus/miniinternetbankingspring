<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
  <head>
<%@ page isELIgnored="false"%>
    <!-- Required meta tags -->
    <title>Mini Internet Banking</title>
    <!-- plugins:css -->
	<link href="<c:url value="/assets/vendors/mdi/css/materialdesignicons.min.css" />" rel="stylesheet">
	<link href="<c:url value="/assets/vendors/css/vendor.bundle.base.css" />" rel="stylesheet">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
	<link href="<c:url value="/assets/css/style.css" />" rel="stylesheet">
    <!-- End layout styles -->
	<link href="<c:url value="/assets/images/favicon.png" />" rel="shortcut icon">
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <jsp:include page="../views/partials/_navbar.jsp">
      	<jsp:param name="nasabah" value="${nasabah}" />
      </jsp:include>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
	    <jsp:include page="../views/partials/_sidebar.jsp">
	    	<jsp:param name="nasabah" value="${nasabah}" />
	    </jsp:include>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">PEMBELIAN - VOUCHER ISI ULANG</h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Pembelian</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Voucher Isi Ulang</li>
                </ol>
              </nav>
            </div>
	        <div class="row" id="proBanner">
	           <div class="col-12">
	           <c:if test="${not empty pesanError[0]}">
	             <span class="d-flex align-items-center purchase-popup">
	               <p>${pesanError[0]}</p>
	             </span>
	           </c:if>
	         </div>
              <div class="col-sm-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">VOUCHER ISI ULANG</h4>
                    <p class="card-description"> PULSA HANDPHONE </p>
                    <form name='kataSandi' action="voucher-submit" method='POST' class="forms-sample">
                      <div class="form-group row">
                        <label for="exampleSelectGender" class="col-sm-3 col-form-label">JENIS VOUCHER</label>
                        <div class="col-sm-9">
	                        <select class="form-control" id="daftarProvider" name="provider">
	                          <option value="null" selected disabled hidden>PILIH PROVIDER</option>
	                          <option value="TELKOMSEL">TELKOMSEL</option>
	                          <option value="XL">XL</option>
	                          <option value="INDOSAT">INDOSAT</option>
	                        </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleSelectGender" class="col-sm-3 col-form-label">NOMINAL VOUCHER</label>
                        <div class="col-sm-9">
	                        <select class="form-control" id="daftarNominal" name="nominal">
	                          <option value="null" selected disabled hidden>--Pilih Nominal--</option>
	                          <option value="10000">10.000</option>
	                          <option value="25000">25.000</option>
	                          <option value="50000">50.000</option>
	                          <option value="100000">100.000</option>
	                          <option value="500000">500.000</option>
	                          <option value="1000000">1.000.000</option>
	                        </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputMobile" class="col-sm-3 col-form-label">NO TELEPON</label>
                        <div class="col-sm-9">
                          <input type="number" class="form-control" id="mobilePhone" name="nomor" placeholder="Mobile number" autocomplete="off" required="true">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputPassword2" class="col-sm-3 col-form-label">MASUKKAN 8 ANGKA INI PADA KEYBCA</label>
                        <div class="col-sm-9">
                          <div class="col-sm-12 row">
	                          <input type="text" class="form-control text-right col-4" value="${random}" disabled>
	                          <input type="text" class="form-control col-8" id="inputToken" value="000000" disabled>
                          </div>
                          <label class="float-right my-2 mr-4">PASTIKAN 6 ANGKA TERAKHIR SESUAI DENGAN NOMOR HANDPHONE ANDA</label>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputConfirmPassword2" class="col-sm-3 col-form-label">RESPON KEYBCA APPLI 2</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control" id="responKey" placeholder="Apply Token" autocomplete="off" required="true">
                        </div>
                      </div>
                      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                      <input type="submit" id="submit" name="submit" value="Kirim" class="btn btn-gradient-info float-right"/>
                      <button class="btn btn-light float-right mr-2">Reset</button>
                    </form>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<c:url value="/assets/vendors/js/vendor.bundle.base.js"/>"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="<c:url value="/assets/js/off-canvas.js"/>"></script>
    <script src="<c:url value="/assets/js/hoverable-collapse.js"/>"></script>
    <script src="<c:url value="/assets/js/misc.js"/>"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
	    $(document).ready(function(){
	    	$('#mobilePhone').keyup(function() {
	    	    var value = $(this).val();
	    	    $('#inputToken').val( value.slice(-6) );
	    	});
    	}).keyup();
	    
	    $(function(){
	    	  $("#submit").attr("disabled","true");
	    	  $("#mobilePhone, #responKey").keyup(function(){
	    	      if($("#mobilePhone").val() && $("#responKey").val())
	    	          $("#submit").removeAttr("disabled");
	    	  });
	    	});
	</script>
    <!-- End custom js for this page -->
  </body>
</html>