<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
  <head>
<%@ page isELIgnored="false"%>
    <!-- Required meta tags -->
    <title>Mini Internet Banking</title>
    <!-- plugins:css -->
	<link href="<c:url value="/assets/vendors/mdi/css/materialdesignicons.min.css" />" rel="stylesheet">
	<link href="<c:url value="/assets/vendors/css/vendor.bundle.base.css" />" rel="stylesheet">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
	<link href="<c:url value="/assets/css/style.css" />" rel="stylesheet">
    <!-- End layout styles -->
	<link href="<c:url value="/assets/images/favicon.png" />" rel="shortcut icon">
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <jsp:include page="../views/partials/_navbar.jsp">
      	<jsp:param name="nasabah" value="${nasabah}" />
      </jsp:include>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../views/partials/_sidebar.html -->
	    <jsp:include page="../views/partials/_sidebar.jsp">
	    	<jsp:param name="nasabah" value="${nasabah}" />
	    </jsp:include>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">KARTU KREDIT - DAFTAR TRANSAKSI</h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Kartu Kredit</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Daftar Transaksi</li>
                </ol>
              </nav>
            </div>
	        <div class="row" id="proBanner">
	           <div class="col-12">
	           <c:if test="${not empty pesanError[2] || not empty pesanError[3] || not empty pesanError[4] || not empty pesanError[5]}">
	             <span class="d-flex align-items-center purchase-popup">
	               <p>${pesanError[2]}${pesanError[3]}${pesanError[4]}${pesanError[5]}</p>
	             </span>
	           </c:if>
	         </div>
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  	<div class="col-12 row">
                  		<div class="col-6"> 
		                    <h4 class="card-title">KARTU KREDIT</h4>
		                    <p class="card-description"> Daftar Transaksi
                  		</div>
                  		<div class="col-6"> 
					         <div class="dropdown float-right">
					            <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton"
					               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bulan ${bulan}</button>
				              	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				               		<a class="dropdown-item" href="${pageContext.request.contextPath}/kartu-kredit/transaksi/1">Januari</a> 
				               		<a class="dropdown-item" href="${pageContext.request.contextPath}/kartu-kredit/transaksi/2">Februari</a>
				               		<a class="dropdown-item" href="${pageContext.request.contextPath}/kartu-kredit/transaksi/3">Maret</a>
				               		<a class="dropdown-item" href="${pageContext.request.contextPath}/kartu-kredit/transaksi/4">April</a>
				            	</div>
					         </div>
                  		</div>
                  	</div>
                    </p>
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th> # </th>
                          <th> Nama Merchant </th>
                          <th> Tanggal Transaksi </th>
                          <th> Nominal Transaksi </th>
                        </tr>
                      </thead>
                      <tbody>
                      
                      	<c:forEach items="${listCCTransaksi}" var="transaksi" varStatus="loop">
                        <tr>
                          <td> ${loop.count} </td>
                          <td> ${transaksi.merchant} </td>
                          <td> ${transaksi.tanggal} </td>
                          <td><fmt:formatNumber pattern="#,###,###,###" value="${transaksi.nominal}" /></td>
                        </tr>
                        </c:forEach>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">BootstrapDash</a>. All rights reserved.</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div></div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<c:url value="/assets/vendors/js/vendor.bundle.base.js"/>"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="<c:url value="/assets/js/off-canvas.js"/>"></script>
    <script src="<c:url value="/assets/js/hoverable-collapse.js"/>"></script>
    <script src="<c:url value="/assets/js/misc.js"/>"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="<c:url value="/assets/js/dashboard.js"/>"></script>
    <script src="<c:url value="/assets/js/todolist.js"/>"></script>
    <!-- End custom js for this page -->
  </body>
</html>