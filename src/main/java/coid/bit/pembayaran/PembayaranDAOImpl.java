package coid.bit.pembayaran;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import coid.bit.nasabah.NasabahModel;
import coid.bit.transaksi.TransaksiModel;

@Repository
@EnableTransactionManagement
@Transactional
public class PembayaranDAOImpl implements PembayaranDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void savePembayaranTelepon(Long id_transaksi, String provider, String nomor, Long nominal) {
		Session session = sessionFactory.getCurrentSession();
		TransaksiModel transaksi = session.byId(TransaksiModel.class).load(id_transaksi);

		PembayaranTeleponModel pembayaranTelepon = new PembayaranTeleponModel();

		pembayaranTelepon.setTransaksi(transaksi);
		pembayaranTelepon.setProvider(provider);
		pembayaranTelepon.setNomor(nomor);
		pembayaranTelepon.setNominal(nominal);

		session.save(pembayaranTelepon);
		
		// log cek query insert
		System.out.println(
				"=======================Transaksi Telepon Dibuat=======================\n"
				+ pembayaranTelepon.getNomor() +
				"\n=======================Transaksi Telepon Dibuat======================="
				);
	}

}
