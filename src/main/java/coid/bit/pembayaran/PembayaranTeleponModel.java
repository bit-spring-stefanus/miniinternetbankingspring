package coid.bit.pembayaran;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import coid.bit.transaksi.TransaksiModel;

@Entity
@Table(name = "T_PEMBAYARAN")
public class PembayaranTeleponModel {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_transaksi", referencedColumnName = "id")
    private TransaksiModel transaksi;

	@Column(name = "PROVIDER")
	private String provider;

	@Column(name = "NOMOR")
	private String nomor;

	@Column(name = "NOMINAL")
	private Long nominal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TransaksiModel getTransaksi() {
		return transaksi;
	}

	public void setTransaksi(TransaksiModel transaksi) {
		this.transaksi = transaksi;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getNomor() {
		return nomor;
	}

	public void setNomor(String nomor) {
		this.nomor = nomor;
	}

	public Long getNominal() {
		return nominal;
	}

	public void setNominal(Long nominal) {
		this.nominal = nominal;
	}
	
}
