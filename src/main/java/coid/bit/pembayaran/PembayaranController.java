package coid.bit.pembayaran;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import coid.bit.transaksi.TransaksiService;

@Controller
@RequestMapping("/pembayaran")
public class PembayaranController {

	@Autowired
	private TransaksiService transaksiService;

	@Autowired
	private PembayaranService pembayaranService;

	@GetMapping("/telepon")
	public String pembayaranPage(HttpServletRequest request, ModelMap model) {
		String random1 = String.valueOf((int) (0 + Math.floor(Math.random() * 9)));
		String random2 = String.valueOf((int) (0 + Math.floor(Math.random() * 9)));
		model.addAttribute("random", random1 + random2);
		return "pembayaran-telepon";
	}
	
	@PostMapping("/telepon-validasi")
	public String validasiPayment(HttpServletRequest request, ModelMap model) {
		String provider = request.getParameter("provider");
		String nominal = request.getParameter("nominal");
		String nomor = request.getParameter("nomor");
		
		model.addAttribute("provider", provider);
		model.addAttribute("telepon", nomor);
		model.addAttribute("nominal", nominal);
		return "pembayaran-validasi";
	}

	@PostMapping("/telepon-submit")
	public String submitPayment(HttpServletRequest request, ModelMap model) {
		String[] errorMessge = new String[2];
		boolean formValid = true;

		if (formValid) {
			Long nominal = Long.valueOf(request.getParameter("nominal"));
			String provider = request.getParameter("provider");
			String notelepon = request.getParameter("nomor");
			
			Long id_nasabah = (long) request.getSession().getAttribute("idNasabah");

			// create transaksi
			Long id_transaksi = transaksiService.saveTransaksi(id_nasabah, "Pembayaran Telepon", nominal);

			// create transaksi telepon
			pembayaranService.savePembayaranTelepon(id_transaksi, provider, notelepon, nominal);
			errorMessge[1] = "SUKSES! Pembayaran Telepon berhasil.";
		} else {
			errorMessge[0] = "GAGAL! Pembayaran Telepon tidak berhasil.";
		}
		model.addAttribute("pesanError", errorMessge);
		return "pembayaran-telepon";
	}

}
