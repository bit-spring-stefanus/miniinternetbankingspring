package coid.bit.pembayaran;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PembayaranServiceImpl implements PembayaranService{
	

	@Autowired
	private PembayaranDAO pembayaranDAO;

	@Override
	public void savePembayaranTelepon(Long id_transaksi, String provider, String notelepon, Long nominal) {
		pembayaranDAO.savePembayaranTelepon(id_transaksi, provider, notelepon, nominal);
	}

}
