package coid.bit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = { "coid.bit.nasabah", "" })
@EnableGlobalMethodSecurity(securedEnabled = true, proxyTargetClass = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		System.out.println("=========================GET USER LOGIN=========================");
		System.out.println(userDetailsService.toString());
		System.out.println("================================================================");
		http
		  	.formLogin()
		  		.loginPage("/login")
		  		.defaultSuccessUrl("/welcome", true)
		  		.failureUrl("/login?error")
				.permitAll()
				.and()
			.exceptionHandling()
				.accessDeniedPage("/denied")
				.and()
			.authorizeRequests()
				.antMatchers("/accounts/resources/**").permitAll()
				.antMatchers("/accounts/edit*").hasRole("NASABAH")
				.antMatchers("/accounts/account*").hasAnyRole("NASABAH", "CSO")
				.antMatchers("/kartu-kredit/**").authenticated()
				.antMatchers("/pembelian/**").authenticated()
				.antMatchers("/pembayaran/**").authenticated()
				.antMatchers("/pengaturan/**").authenticated()
				.antMatchers("/welcome").authenticated()
				.antMatchers("/").authenticated()
				.and()
			.logout()
				.logoutSuccessUrl("/login?logout")
				.permitAll()
				.and()
			.csrf();
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		//pass = secret
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
//		auth.inMemoryAuthentication().passwordEncoder(passwordEncoder).withUser("user")
//				.password(passwordEncoder.encode("user")).roles("USER").and().withUser("admin")
//				.password(passwordEncoder.encode("admin")).roles("USER", "ADMIN");
	}
}
