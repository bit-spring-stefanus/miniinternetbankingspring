package coid.bit.nasabah;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import coid.bit.transaksi.TransaksiModel;
import coid.bit.transaksi.TransaksiService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

@Controller
public class NasabahController {

	@Autowired
	private NasabahService nasabahService;

	/* Autowired messageSource bean defined in ContextConfig.java */
	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "/login", method = { RequestMethod.GET, RequestMethod.POST })
	public String loginPage(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, Model model) {
		String errorMessge = null;
		if (error != null) {
			errorMessge = "Username or Password is incorrect !!";
		}
		if (logout != null) {
			errorMessge = "You have been successfully logged out !!";
		}
		model.addAttribute("pesanError", errorMessge);
		return "login";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout=true";
	}

	@GetMapping("/welcome")
	public String welcomePage(HttpSession session, ModelMap model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		NasabahModel nasabah = nasabahService.getNasabah(auth.getName());
		session.setAttribute("idNasabah", nasabah.getId());
		model.addAttribute("nasabah", nasabah);
		return "welcome";
	}

	@GetMapping("/pengaturan/ganti-bahasa")
	public String changeLanguagePage(HttpServletRequest request, Model model) {
		return "ganti-bahasa";
	}

	@PostMapping("/pengaturan/simpan-bahasa")
	public String changeLanguageSave(HttpServletRequest request, Model model) {
		String options = request.getParameter("optionsRadios");
		return "redirect:/welcome?lang="+options;
	}

	@GetMapping("/pengaturan/ganti-password")
	public String changePasswordPage(HttpServletRequest request, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		System.out.println(auth.getName());
		return "ganti-password";
	}

	@PostMapping("/pengaturan/simpan-password")
	public String changePasswordSave(HttpServletRequest request, ModelMap model) {
		String[] errorMessge = new String[6];
		boolean formValid = true;

		// Form Validation
		if (request.getParameter("passwordLama").isEmpty()) {
			errorMessge[0] = "*Kolom kata sandi lama harus diisi";
			formValid = false;
		}
		if (request.getParameter("passwordBaru").isEmpty()) {
			errorMessge[1] = "*Kolom kata sandi baru harus diisi";
			formValid = false;
		}
		if (formValid) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			NasabahModel nasabah = nasabahService.getNasabah(auth.getName());
			PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			if (!passwordEncoder.matches(request.getParameter("passwordLama"), nasabah.getKatasandi())) {
				errorMessge[2] = "GAGAL! Kata sandi lama tidak sesuai";
			} else {
				if (request.getParameter("passwordBaru").length() < 8) {
					errorMessge[3] = "GAGAL! Kata sandi baru minimal 8 karakter";
				} else if (!request.getParameter("passwordBaru")
						.equals(request.getParameter("passwordBaruKonfirmasi"))) {
					errorMessge[4] = "GAGAL! Kata sandi baru tidak sesuai";
				} else {
					nasabahService.changePassword(nasabah.getId(),
							passwordEncoder.encode(request.getParameter("passwordBaru")));
					errorMessge[5] = "SUKSES! Kata sandi berhasil diganti";
				}
			}
		}
		model.addAttribute("pesanError", errorMessge);
		return "ganti-password";
	}

}