package coid.bit.nasabah;

import java.util.List;

public interface NasabahDAO {
	public List<NasabahModel> getNasabah();

	public void saveNasabah(NasabahModel theNasabah);

	public NasabahModel getNasabah(long theId);

	public void deleteNasabah(long theId);
	
	public NasabahModel getNasabahByUsername(String username);
	
	public void updateKataSandi(long theId, String newPassword);

}
