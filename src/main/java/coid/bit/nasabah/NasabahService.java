package coid.bit.nasabah;

import java.util.List;

public interface NasabahService {
	public List<NasabahModel> getNasabah();

	public void saveNasabah(NasabahModel theNasabah);

	public NasabahModel getNasabah(long theId);

	public void deleteNasabah(long theId);
	
	public NasabahModel getNasabah(String username);

	public void changePassword(long theId, String newPassword);
	
}
