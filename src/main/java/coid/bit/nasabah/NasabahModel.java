package coid.bit.nasabah;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import coid.bit.kartuKredit.TagihanCCModel;
import coid.bit.kartuKredit.TrxCCModel;
import coid.bit.transaksi.TransaksiModel;

@Entity
@Table(name = "T_NASABAH")
public class NasabahModel {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "NAMA")
	private String nama;

	@Column(name = "USERNAME")
	private String username;

	@Column(name = "KATASANDI")
	private String katasandi;

    @OneToMany(mappedBy = "nasabah",
            cascade = javax.persistence.CascadeType.ALL,
            fetch = javax.persistence.FetchType.LAZY,
            orphanRemoval = true)
    private List<TransaksiModel> transaksi;
    
    @OneToMany(mappedBy = "nasabah",
            cascade = javax.persistence.CascadeType.ALL,
            fetch = javax.persistence.FetchType.LAZY,
            orphanRemoval = true)
    private List<TagihanCCModel> tagihan;
    
    @OneToMany(mappedBy = "nasabah",
            cascade = javax.persistence.CascadeType.ALL,
            fetch = javax.persistence.FetchType.LAZY,
            orphanRemoval = true)
    private List<TrxCCModel> trxCC;

	public Long getId() {
		return id;
	}

	public String getNama() {
		return nama;
	}

	public String getUsername() {
		return username;
	}

	public String getKatasandi() {
		return katasandi;
	}

	public void setKatasandi(String katasandi) {
		this.katasandi = katasandi;
	}

	public void setTransaksi(List<TransaksiModel> transaksi) {
		this.transaksi = transaksi;
	}
	
//
//	public Set<NasabahRoleModel> getUserRole() {
//		return userRole;
//	}
//
//	public void setUserRole(Set<NasabahRoleModel> userRole) {
//		this.userRole = userRole;
//	}
//
//	public NasabahModel(String username, String katasandi,  Set<NasabahRoleModel> userRole) {
//		this.username = username;
//		this.katasandi = katasandi;
//		this.userRole = userRole;
//	}

}
