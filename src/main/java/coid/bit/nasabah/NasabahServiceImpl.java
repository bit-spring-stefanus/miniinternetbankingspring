package coid.bit.nasabah;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NasabahServiceImpl implements NasabahService, UserDetailsService {

	@Autowired
	private NasabahDAO nasabahDAO;

	@Override
	@Transactional
	public List<NasabahModel> getNasabah() {
		return nasabahDAO.getNasabah();
	}

	@Override
	@Transactional
	public void saveNasabah(NasabahModel theCustomer) {
		nasabahDAO.saveNasabah(theCustomer);
	}

	@Override
	@Transactional
	public NasabahModel getNasabah(long theId) {
		return nasabahDAO.getNasabah(theId);
	}
	
	@Override
	@Transactional
	public NasabahModel getNasabah(String username) {
		return nasabahDAO.getNasabahByUsername(username);
	}

	@Override
	@Transactional
	public void deleteNasabah(long theId) {
		nasabahDAO.deleteNasabah(theId);
	}

	@Override
	@Transactional
	public void changePassword(long theId, String newPassword) {
		nasabahDAO.updateKataSandi(theId, newPassword);
	}

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		NasabahModel user = nasabahDAO.getNasabahByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException("Bad credentials");
		}

		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		grantedAuthorities.add(new SimpleGrantedAuthority("NASABAH"));
		grantedAuthorities.add(new SimpleGrantedAuthority("CSO"));

		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getKatasandi(),
				grantedAuthorities);

	}
	
}
