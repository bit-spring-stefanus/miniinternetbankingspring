package coid.bit.transaksi;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import coid.bit.nasabah.NasabahModel;

@Repository
@EnableTransactionManagement
@Transactional
public class TransaksiDAOImpl implements TransaksiDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<TransaksiModel> getAllTransaksiByNasabah(long id_nasabah) {
		List<TransaksiModel> transaksi = new ArrayList<TransaksiModel>();
		try {
			transaksi = sessionFactory.getCurrentSession().createQuery("FROM TransaksiModel WHERE nasabah.id=?1")
					.setParameter(1, id_nasabah)
					.list();
			
			// log cek query select
			System.out.println(
					"========================Query List Transaksi=========================\n"
					+ transaksi.toString() +
					"\n========================Query List Transaksi========================="
					);
			
		} catch (Exception e) {
			System.out.println("Tidak ADA!");
		}
		if (transaksi.size() > 0) {
			return transaksi;
		} else {
			return null;
		}
	}

	@Override
	public TransaksiModel getTransaksi(long theId) {
		Session session = sessionFactory.getCurrentSession();
		TransaksiModel transaksi = session.byId(TransaksiModel.class).load(theId);
		
		// log cek query select
		System.out.println(
				"========================Query Transaksi=========================\n"
				+ transaksi.toString() +
				"\n========================Query Transaksi========================="
				);			
		
		return transaksi;
	}

	@Override
	public Long saveTransaksi(long id_nasabah, String jenis, Long nominal) {
		Session session = sessionFactory.getCurrentSession();
		NasabahModel nasabah = session.byId(NasabahModel.class).load(id_nasabah);
		TransaksiModel transaksi = new TransaksiModel();

		transaksi.setNasabah(nasabah);
		transaksi.setJenis(jenis);
		transaksi.setNominal(nominal);
		session.save(transaksi);
		
		// log cek query insert
		System.out.println(
				"========================Transaksi Dibuat=========================\n"
				+ transaksi.getJenis() +
				"\n========================Transaksi Dibuat========================="
				);
		
		return transaksi.getId();
	}

}
