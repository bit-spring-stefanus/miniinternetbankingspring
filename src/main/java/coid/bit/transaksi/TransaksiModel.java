package coid.bit.transaksi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.CreationTimestamp;

import coid.bit.kartuKredit.TrxCCModel;
import coid.bit.nasabah.NasabahModel;
import coid.bit.pembayaran.PembayaranTeleponModel;
import coid.bit.pembelian.TrxVoucherModel;

@Entity
@Table(name = "T_TRANSAKSI")
public class TransaksiModel {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//id_nasabah
    @ManyToOne(cascade = javax.persistence.CascadeType.ALL)
    @JoinColumn(name="id_nasabah", referencedColumnName = "id")
    private NasabahModel nasabah;

	@Column(name = "JENIS")
	private String jenis;

	@Column(name = "NOMINAL")
	private Long nominal;
	
	@CreationTimestamp
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "TANGGAL")
	private Date tanggal;
	
	@OneToOne(mappedBy = "transaksi",
            cascade = javax.persistence.CascadeType.ALL,
            fetch = javax.persistence.FetchType.LAZY,
            orphanRemoval = true)
    private TrxVoucherModel trxVoucher;
	
	@OneToOne(mappedBy = "transaksi",
            cascade = javax.persistence.CascadeType.ALL,
            fetch = javax.persistence.FetchType.LAZY,
            orphanRemoval = true)
    private PembayaranTeleponModel pembayaranTelepon;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public Long getNominal() {
		return nominal;
	}

	public void setNominal(Long nominal) {
		this.nominal = nominal;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public NasabahModel getNasabah() {
		return nasabah;
	}

	public void setNasabah(NasabahModel nasabah) {
		this.nasabah = nasabah;
	}
	
}
