package coid.bit.transaksi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import coid.bit.nasabah.NasabahModel;

@Service("transaksiService")
public class TransaksiServiceImpl implements TransaksiService {

	@Autowired
	private TransaksiDAO transaksiDAO;

	@Override
	@Transactional
	public List<TransaksiModel> getAllTransaksiByNasabah(long id_nasabah) {
		List<TransaksiModel> transaksi = transaksiDAO.getAllTransaksiByNasabah(id_nasabah);
		return transaksi;
	}

	@Override
	@Transactional
	public TransaksiModel getTransaksi(long theId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long saveTransaksi(long id_nasabah, String jenis, Long nominal) {
		return transaksiDAO.saveTransaksi(id_nasabah, jenis, nominal);
	}
}
