package coid.bit.transaksi;

import java.util.List;

import coid.bit.nasabah.NasabahModel;

public interface TransaksiService {

	List<TransaksiModel> getAllTransaksiByNasabah(long id_nasabah);

	TransaksiModel getTransaksi(long theId);

	Long saveTransaksi(long id_nasabah, String jenis, Long nominal);
}
