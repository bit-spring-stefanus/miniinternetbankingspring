package coid.bit.pembelian;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import coid.bit.transaksi.TransaksiModel;

@Entity
@Table(name = "T_TRANSAKSI_VOUCHER")
public class TrxVoucherModel {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_transaksi", referencedColumnName = "id")
    private TransaksiModel transaksi;

	@Column(name = "PROVIDER")
	private String provider;

	@Column(name = "NOMINAL")
	private Long nominal;

	@Column(name = "NOMOR")
	private String nomor;
	
	@Column(name = "KODEVOUCHER")
	private String kodevoucher = UUID.randomUUID().toString();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TransaksiModel getTransaksi() {
		return transaksi;
	}

	public void setTransaksi(TransaksiModel transaksi) {
		this.transaksi = transaksi;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public Long getNominal() {
		return nominal;
	}

	public void setNominal(Long nominal) {
		this.nominal = nominal;
	}

	public String getNomor() {
		return nomor;
	}

	public void setNomor(String nomor) {
		this.nomor = nomor;
	}

	public String getKodevoucher() {
		return kodevoucher;
	}

	public void setKodevoucher(String kodevoucher) {
		this.kodevoucher = kodevoucher;
	}
	
	
//
//	public Set<NasabahRoleModel> getUserRole() {
//		return userRole;
//	}
//
//	public void setUserRole(Set<NasabahRoleModel> userRole) {
//		this.userRole = userRole;
//	}
//
//	public NasabahModel(String username, String katasandi,  Set<NasabahRoleModel> userRole) {
//		this.username = username;
//		this.katasandi = katasandi;
//		this.userRole = userRole;
//	}

}
