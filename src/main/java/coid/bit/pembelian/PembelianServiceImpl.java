package coid.bit.pembelian;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PembelianServiceImpl implements PembelianService{
	
	@Autowired
	private PembelianDAO pembelianDAO;

	@Override
	public void saveTransaksiVoucher(Long id_transaksi, String provider, String notelepon, Long nominal) {
		pembelianDAO.saveTransaksiVoucher(id_transaksi, provider, notelepon, nominal);
	}

}
