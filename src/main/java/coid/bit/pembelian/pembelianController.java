package coid.bit.pembelian;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import coid.bit.transaksi.TransaksiService;

@Controller
@RequestMapping("/pembelian")
public class pembelianController {

	@Autowired
	private TransaksiService transaksiService;

	@Autowired
	private PembelianService pembelianService;

	@GetMapping("/voucher")
	public String voucherPage(HttpServletRequest request, ModelMap model) {
		String random1 = String.valueOf((int) (0 + Math.floor(Math.random() * 9)));
		String random2 = String.valueOf((int) (0 + Math.floor(Math.random() * 9)));
		model.addAttribute("random", random1 + random2);
		return "pembelian-voucher";
	}

	@PostMapping("/voucher-submit")
	public String voucherSave(HttpServletRequest request, ModelMap model) {
		String[] errorMessge = new String[1];
		String provider, notelepon;
		Long nominal;
		boolean formValid = true;

		provider = String.valueOf(request.getParameter("provider"));
		nominal = Long.valueOf(request.getParameter("nominal"));
		notelepon = String.valueOf(request.getParameter("nomor"));

		if (formValid) {
			Long id_nasabah = (long) request.getSession().getAttribute("idNasabah");

			// create transaksi
			Long id_transaksi = transaksiService.saveTransaksi(id_nasabah, "Voucher", nominal);

			// create transaksi voucher
			pembelianService.saveTransaksiVoucher(id_transaksi, provider, notelepon, nominal);
			errorMessge[0] = "SUKSES! Pembelian voucher berhasil.";
		}
		String random1 = String.valueOf((int) (0 + Math.floor(Math.random() * 9)));
		String random2 = String.valueOf((int) (0 + Math.floor(Math.random() * 9)));
		model.addAttribute("random", random1 + random2);

		model.addAttribute("pesanError", errorMessge);
		return "pembelian-voucher";
	}

}
