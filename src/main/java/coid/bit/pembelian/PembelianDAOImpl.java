package coid.bit.pembelian;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import coid.bit.nasabah.NasabahModel;
import coid.bit.transaksi.TransaksiModel;

@Repository
@EnableTransactionManagement
@Transactional
public class PembelianDAOImpl implements PembelianDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void saveTransaksiVoucher(Long id_transaksi, String provider, String nomor, Long nominal) {
		Session session = sessionFactory.getCurrentSession();
		TransaksiModel transaksi = session.byId(TransaksiModel.class).load(id_transaksi);

		TrxVoucherModel trxVoucher = new TrxVoucherModel();

		trxVoucher.setTransaksi(transaksi);
		trxVoucher.setProvider(provider);
		trxVoucher.setNomor(nomor);
		trxVoucher.setNominal(nominal);

		session.save(trxVoucher);
		
		// log cek query insert
		System.out.println(
				"=======================Transaksi Voucher Dibuat=======================\n"
				+ trxVoucher.getKodevoucher() +
				"\n=======================Transaksi Voucher Dibuat======================="
				);
	}

}
