package coid.bit.kartuKredit;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import org.hibernate.annotations.CreationTimestamp;

import coid.bit.nasabah.NasabahModel;

@Entity
@Table(name = "T_TRANSAKSI_CC")
public class TrxCCModel {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	// id_tagihan
	@ManyToOne(cascade = javax.persistence.CascadeType.ALL)
	@JoinColumn(name = "id_tagihan", referencedColumnName = "id")
	private TagihanCCModel tagihanCC;
	
	// id_nasabah
    @ManyToOne(cascade = javax.persistence.CascadeType.ALL)
    @JoinColumn(name="id_nasabah", referencedColumnName = "id")
    private NasabahModel nasabah;

	@Column(name = "MERCHANT")
	private String merchant;

	@Column(name = "NOMINAL")
	private Long nominal;
	
	@CreationTimestamp
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
	@Column(name = "TANGGAL")
	private Date tanggal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TagihanCCModel getTagihanCC() {
		return tagihanCC;
	}

	public void setTagihanCC(TagihanCCModel tagihanCC) {
		this.tagihanCC = tagihanCC;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public Long getNominal() {
		return nominal;
	}

	public void setNominal(Long nominal) {
		this.nominal = nominal;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}
	
	
}
