package coid.bit.kartuKredit;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class KartuKreditServiceImpl implements KartuKreditService {

	@Autowired
	KartuKreditDAO kartuKreditDAO;

	@Override
	@Transactional
	public List<TrxCCModel> getAllTrxCCByTagihan(long theId) {
		return kartuKreditDAO.getAllTrxCCByTagihan(theId);
	}

	@Override
	@Transactional
	public List<TrxCCModel> getAllTrxCCByNasabah(long theId) {
		return kartuKreditDAO.getAllTrxCCByNasabah(theId);
	}

	@Override
	@Transactional
	public List<TrxCCModel> getAllTrxCCByNasabahAndMonth(long theId, int month) {
		return kartuKreditDAO.getAllTrxCCByNasabahAndMonth(theId, month);
	}

	@Override
	@Transactional
	public void updateTagihan(TagihanCCModel tagihanCC) {
		kartuKreditDAO.updateTagihan(tagihanCC);
	}

	@Override
	@Transactional
	public TagihanCCModel getTagihanByMonth(long theId, int month) {
		return kartuKreditDAO.getTagihanByMonth(theId, month);
	}

	@Override
	@Transactional
	public TagihanCCModel getTagihanCCModel(long theId) {
		return kartuKreditDAO.getTagihanCCModel(theId);
	}

}
