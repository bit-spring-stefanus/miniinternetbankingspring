package coid.bit.kartuKredit;

import java.util.Date;
import java.util.List;
import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.ZoneId;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/kartu-kredit")
public class KartuKreditController {

	@Autowired
	private KartuKreditService kartuKreditService;

	@GetMapping("/transaksi")
	public String transaksiPage(HttpServletRequest request, ModelMap model) {
		Long id_nasabah = (long) request.getSession().getAttribute("idNasabah");
		List<TrxCCModel> listCC = kartuKreditService.getAllTrxCCByNasabah(id_nasabah);
		model.addAttribute("listCCTransaksi", listCC);
		return "cc_transaksi";
	}

	@GetMapping("/transaksi/{bulan}")
	public String transaksiByMonthPage(@PathVariable Integer bulan, HttpServletRequest request, ModelMap model) {
		Long id_nasabah = (long) request.getSession().getAttribute("idNasabah");
		List<TrxCCModel> listCC = kartuKreditService.getAllTrxCCByNasabahAndMonth(id_nasabah, Integer.valueOf(bulan));
		String monthString = new DateFormatSymbols().getMonths()[bulan - 1];
		model.addAttribute("bulan", monthString);
		model.addAttribute("listCCTransaksi", listCC);
		return "cc_transaksi";
	}

	@GetMapping("/tagihan")
	public String tagihanPage(HttpServletRequest request, ModelMap model) {
		Long id_nasabah = (long) request.getSession().getAttribute("idNasabah");
		Long toTagihan, miBayar, tghDhDibayar;
		Date tgJthTempo, date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int bulan = localDate.getMonthValue();
		String monthString = new DateFormatSymbols().getMonths()[bulan - 1];
		List<TrxCCModel> listCC = kartuKreditService.getAllTrxCCByNasabahAndMonth(id_nasabah,
				Integer.valueOf(bulan - 1));
		TagihanCCModel tagihanCC = kartuKreditService.getTagihanByMonth(id_nasabah, bulan);

		try {
			toTagihan = tagihanCC.getTagihan() - tagihanCC.getTagihanDiBayar();
			miBayar = toTagihan * 30 / 100;
			tgJthTempo = tagihanCC.getBatas_pembayaran();
			tghDhDibayar = tagihanCC.getTagihanDiBayar();
		} catch (Exception e) {
			return "redirect:/welcome";
		}

		model.addAttribute("id_tagihan", tagihanCC.getId());
		model.addAttribute("intBulan", bulan);
		model.addAttribute("periode", monthString);
		model.addAttribute("toTagihanAll", tagihanCC.getTagihan());
		model.addAttribute("toTagihan", toTagihan);
		model.addAttribute("tagihanSdhDibayar", tghDhDibayar);
		model.addAttribute("miBayar", miBayar);
		model.addAttribute("tgJthTempo", tgJthTempo);

		model.addAttribute("listCCTransaksi", listCC);
		return "cc_tagihan";
	}

	@PostMapping("/tagihan-submit")
	public String tagihanSubmit(HttpServletRequest request, ModelMap model) {
		String[] errorMessge = new String[2];
		boolean formValid = true;

		Long nominalBayar = Long.valueOf(request.getParameter("nominalBayar"));
		Long minimalBayar = Long.valueOf(request.getParameter("miBayar"));
		Long theId = Long.valueOf(request.getParameter("id_tagihan"));

		TagihanCCModel tagihanCC = kartuKreditService.getTagihanCCModel(theId);

		// Validasi Pembayaran
		if (nominalBayar < minimalBayar) {
			errorMessge[0] = "GAGAL! Nominal Pembayaran Harus Lebih Besar Dari Minimal Pembayaran";
			formValid = false;
		}

		if (formValid) {
			Long tempPemb = tagihanCC.getTagihanDiBayar();
			tagihanCC.setTagihanDiBayar(tempPemb + nominalBayar);
			kartuKreditService.updateTagihan(tagihanCC);
			errorMessge[1] = "SUKSES! Pembayaran Kartu Kredit Berhasil.";
		}

		Long toTagihan = tagihanCC.getTagihan() - tagihanCC.getTagihanDiBayar();
		Long miBayar = toTagihan * 30 / 100;
		Date tgJthTempo = tagihanCC.getBatas_pembayaran();
		model.addAttribute("id_tagihan", tagihanCC.getId());

		List<TrxCCModel> listCC = kartuKreditService.getAllTrxCCByTagihan(theId);

		Long tghDhDibayar = tagihanCC.getTagihanDiBayar();

		String periode = request.getParameter("periode");

		model.addAttribute("periode", periode);
		model.addAttribute("toTagihanAll", tagihanCC.getTagihan());
		model.addAttribute("toTagihan", toTagihan);
		model.addAttribute("tagihanSdhDibayar", tghDhDibayar);
		model.addAttribute("miBayar", miBayar);
		model.addAttribute("tgJthTempo", tgJthTempo);

		model.addAttribute("listCCTransaksi", listCC);
		
		model.addAttribute("pesanError", errorMessge);
		return "cc_tagihan";
	}

}
