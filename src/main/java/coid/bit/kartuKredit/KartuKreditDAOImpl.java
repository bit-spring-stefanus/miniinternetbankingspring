package coid.bit.kartuKredit;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import coid.bit.nasabah.NasabahModel;

@Repository
@EnableTransactionManagement
@Transactional
public class KartuKreditDAOImpl implements KartuKreditDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	@SuppressWarnings("unchecked")
	public List<TrxCCModel> getAllTrxCCByTagihan(long theId) {
		List<TrxCCModel> transaksiCC = new ArrayList<TrxCCModel>();
		try {
			transaksiCC = sessionFactory.getCurrentSession().createQuery("FROM TrxCCModel WHERE tagihanCC.id=?1")
					.setParameter(1, theId).list();

			// log cek query select
			System.out.println("========================Query Get Transaksi CC=========================\n"
					+ transaksiCC.toString() + " NAMA : " + transaksiCC.get(0).getMerchant()
					+ "\n========================Query Get Transaksi CC=========================");

		} catch (Exception e) {
			// log cek query select
			System.out.println("========================Query Get Transaksi CC=========================\n"
					+ "TRANSAKSI CC TIDAK DITEMUKAN !"
					+ "\n========================Query Get Transaksi CC=========================");
		}
		if (transaksiCC.size() > 0) {
			return transaksiCC;
		} else {
			return null;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TrxCCModel> getAllTrxCCByNasabah(long theId) {
		List<TrxCCModel> transaksiCC = new ArrayList<TrxCCModel>();
		try {

			transaksiCC = sessionFactory.getCurrentSession().createQuery("FROM TrxCCModel WHERE nasabah.id=?1")
					.setParameter(1, theId).list();

			// log cek query select
			System.out.println(
					"========================Query Get Transaksi CC=========================\n"
					+ transaksiCC.toString() + " NAMA : " + transaksiCC.get(0).getMerchant()
					+ "\n========================Query Get Transaksi CC========================="
					);

		} catch (Exception e) {
			// log cek query select
			System.out.println(
					"========================Query Get Transaksi CC=========================\n"
					+ "TRANSAKSI CC TIDAK DITEMUKAN !"
					+ "\n========================Query Get Transaksi CC========================="
					);
		}
		if (transaksiCC.size() > 0) {
			return transaksiCC;
		} else {
			return null;
		}
	}

	@Override
	public List<TrxCCModel> getAllTrxCCByNasabahAndMonth(long theId, int month) {
		List<TrxCCModel> transaksiCC = getAllTrxCCByNasabah(theId);
		try {
			int count = transaksiCC.size();
			List<TrxCCModel> tempTrxCC = new ArrayList<TrxCCModel>();
			for (int i = 0; i < count; i++) {

				// Convert Date to LocalDate
				Date date = transaksiCC.get(i).getTanggal();
				LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

				// Get Month Value
				Integer x = localDate.getMonthValue();

				// Select Object with month
				if (x == month) {
					System.out.println(transaksiCC.get(i).getMerchant());
					tempTrxCC.add(transaksiCC.get(i));
				}
			}
			return tempTrxCC;
		} catch (Exception e) {
			System.out.println("===============Tagihan Bulanan Tidak Ditemukan===============");
			return null;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public TagihanCCModel getTagihanByMonth(long theId, int month) {
		List<TagihanCCModel> tagihanCC = new ArrayList<TagihanCCModel>();
		tagihanCC = sessionFactory.getCurrentSession().createQuery("FROM TagihanCCModel WHERE nasabah.id=?1")
				.setParameter(1, theId).list();
		if (tagihanCC.size() < 1) {
			return null;
		} else {
			int count = tagihanCC.size();
			TagihanCCModel tempTagihanCC = null;
			for (int i = 0; i < count; i++) {

				// Convert Date to LocalDate
				Date date = tagihanCC.get(i).getBatas_pembayaran();
				LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

				// Get Month Value
				Integer x = localDate.getMonthValue();

				// Select Object with month
				if (x == month) {
					System.out.println(tagihanCC.get(i).getBatas_pembayaran());
					tempTagihanCC = tagihanCC.get(i);
				}
			}
			if (tempTagihanCC == null) {
				System.out.println("null");
				return null;
			} else {
				return tempTagihanCC;
			}
		}
	}

	@Override
	public TagihanCCModel getTagihanCCModel(long theId) {
		Session session = sessionFactory.getCurrentSession();
		TagihanCCModel tagihanCCModel = session.byId(TagihanCCModel.class).load(theId);
		return tagihanCCModel;
	}

	@Override
	public void updateTagihan(TagihanCCModel tagihanCC) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.saveOrUpdate(tagihanCC);

		// log cek query insert/update
		System.out.println(
				"========================Update Tagihan Dibayar=========================\n"
				+ tagihanCC.getTagihan() + "DIBAYAR : " + tagihanCC.getTagihanDiBayar()
				+ "\n========================Update Tagihan Dibayar========================="
				);
	}

}
