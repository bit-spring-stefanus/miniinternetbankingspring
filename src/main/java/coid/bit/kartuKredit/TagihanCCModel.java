package coid.bit.kartuKredit;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import coid.bit.nasabah.NasabahModel;

@Entity
@Table(name = "T_TAGIHAN_CC")
public class TagihanCCModel {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//id_nasabah
    @ManyToOne(cascade = javax.persistence.CascadeType.ALL)
    @JoinColumn(name="id_nasabah", referencedColumnName = "id")
    private NasabahModel nasabah;
    
	@Column(name = "TAGIHAN")
	private Long tagihan;

	@Column(name = "TAGIHAN_DIBAYAR")
	private Long tagihanDiBayar;
	
	@Column(name = "BATAS_PEMBAYARAN")
	private Date batas_pembayaran;

	@Column(name = "LUNAS")
	private boolean lunas;
	
    @OneToMany(mappedBy = "tagihanCC",
            cascade = javax.persistence.CascadeType.ALL,
            fetch = javax.persistence.FetchType.LAZY,
            orphanRemoval = true)
    private List<TrxCCModel> trxCCModel;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NasabahModel getNasabah() {
		return nasabah;
	}

	public void setNasabah(NasabahModel nasabah) {
		this.nasabah = nasabah;
	}

	public Long getTagihan() {
		return tagihan;
	}

	public void setTagihan(Long tagihan) {
		this.tagihan = tagihan;
	}

	public Long getTagihanDiBayar() {
		return tagihanDiBayar;
	}

	public void setTagihanDiBayar(Long tagihanDiBayar) {
		this.tagihanDiBayar = tagihanDiBayar;
	}

	public Date getBatas_pembayaran() {
		return batas_pembayaran;
	}

	public void setBatas_pembayaran(Date batas_pembayaran) {
		this.batas_pembayaran = batas_pembayaran;
	}

	public boolean isLunas() {
		return lunas;
	}

	public void setLunas(boolean lunas) {
		this.lunas = lunas;
	}

	public List<TrxCCModel> getTrxCCModel() {
		return trxCCModel;
	}

	public void setTrxCCModel(List<TrxCCModel> trxCCModel) {
		this.trxCCModel = trxCCModel;
	}
    
}
