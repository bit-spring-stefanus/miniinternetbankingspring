package coid.bit.kartuKredit;

import java.util.List;

public interface KartuKreditDAO {

	List<TrxCCModel> getAllTrxCCByNasabah(long theId);

	List<TrxCCModel> getAllTrxCCByNasabahAndMonth(long theId, int month);
	
	public TagihanCCModel getTagihanCCModel(long theId);
	
	// buat dapet transaksi per bulan
	List<TrxCCModel> getAllTrxCCByTagihan(long theId);
	
	// buat bayar tagihan
	void updateTagihan(TagihanCCModel tagihanCC);

	// get info tagihan
	TagihanCCModel getTagihanByMonth(long theId, int month);
}
